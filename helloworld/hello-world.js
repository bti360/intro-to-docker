var http = require('http');
var Router = require('node-router');

var router = Router();
var route = router.push;

route(function(req, resp, next) {
    console.log('-----------------------------');
    console.log('Request Path: ' + req.path);
    console.log('Request Query: ' + JSON.stringify(req.query));
    next();
});

route('/hello', function(req, resp, next) {
    var name = 'World';
    if (req.query.name !== undefined && req.query.name !== null && req.query.name.length > 0) {
        name = req.query.name;
    }
    resp.send(200, 'Hello, ' + name + '!')
});

route(function (req, resp, next) {
    resp.send(404, 'Not Found');
});

var server = http.createServer(router).listen(3000);
console.log("Server running at http://127.0.0.1:3000/");
