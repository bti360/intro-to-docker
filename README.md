# BTI360 Intro to Docker

This repository is a companion to the BTI360 Intro to Docker video series.

## Pet Store API

The video series uses Swagger's Pet Store API to demonstrate running an application
inside a Docker container.

The API code was taken from the [swagger-codegen](https://github.com/swagger-api/swagger-codegen)
project's NodeJS example server, distributed under the Apache 2 license.
